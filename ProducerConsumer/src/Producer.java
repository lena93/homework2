public class Producer implements Runnable{

    MyArrayList myArrayList;
    public Producer(MyArrayList myArrayList) {
        this.myArrayList = myArrayList;
    }
    int from = 0;
    int to = 100;
    public void run(){
        Integer c;
        for (int i = 0; i < 200; i++) {
            c = (int) (from + Math.random() * to);
            myArrayList.push(c);
            System.out.println("Producer" + i + ": " + c);
            try {
                Thread.sleep((int)(Math.random()*300));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
