import java.util.ArrayList;
import java.util.List;

public class MyArrayList {
    MyArrayList(){}

    public static List buffer = new ArrayList();

    public synchronized void push(Integer c){
        this.notify();
        Integer charObj = new Integer(c);
        buffer.add(charObj);
    }

    public synchronized Integer pop(){
        Integer c;
        while(buffer.size() == 0){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        c = ((Integer)buffer.remove(buffer.size()-1)).intValue();
        return c;
    }
}
