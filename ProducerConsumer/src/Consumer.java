public class Consumer implements Runnable{
    MyArrayList myArrayList;
    public Consumer(MyArrayList myArrayList) {
        this.myArrayList = myArrayList;
    }

    public void run() {
        Integer c;
        for (int i = 0; i < 200; i++) {

            c = myArrayList.pop();
            System.out.println("Consumer" + i + ": " + c);
            try {
                System.out.println("ArrayList size = " + myArrayList.buffer.size());
                Thread.sleep((int)(Math.random()*300));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
