public class Main {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();

        Producer p1 = new Producer(myArrayList);
        Thread prodT1 = new Thread(p1);
        prodT1.start();


        Consumer c1 = new Consumer(myArrayList);
        Thread consT1 = new Thread(c1);
        consT1.start();
    }
}
